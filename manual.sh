#! /bin/bash

#verificar se itaipu.pbf está em ~/data e se possui permissão root

#https://switch2osm.org/manually-building-a-tile-server-16-04-2-lts/
#https://www.a2hosting.com/kb/developer-corner/postgresql/managing-postgresql-databases-and-users-from-the-command-line

renderaccount="root"

apt-get update && apt-get -y install sudo vim

sudo apt -y install libboost-all-dev git-core tar unzip wget bzip2 build-essential autoconf libtool libxml2-dev libgeos-dev libgeos++-dev libpq-dev libbz2-dev libproj-dev munin-node munin libprotobuf-c0-dev protobuf-c-compiler libfreetype6-dev libpng12-dev libtiff5-dev libicu-dev libgdal-dev libcairo-dev libcairomm-1.0-dev apache2 apache2-dev libagg-dev liblua5.2-dev ttf-unifont lua5.1 liblua5.1-dev libgeotiff-epsg

sudo apt-get -y install postgresql postgresql-contrib postgis postgresql-9.5-postgis-2.2

runuser -l postgres -c '/etc/init.d/postgresql start'

runuser -l postgres -c 'createuser root'

runuser -l postgres -c 'createdb -O root gis'

echo " \c gis"

echo " CREATE EXTENSION postgis;"
runuser -l postgres -c 'psql gis -c "create extension postgis"'

echo " CREATE EXTENSION hstore;"
runuser -l postgres -c 'psql gis -c "create extension hstore"'

echo " ALTER TABLE geometry_columns OWNER TO root"
runuser -l postgres -c 'psql gis -c "alter table geometry_columns owner to root"'

echo " \q"

chmod +x manual2.sh

echo "./manual2.sh"

#runuser -l postgres -c 'psql'

./manual2.sh


