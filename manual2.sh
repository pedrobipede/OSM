#! /bin/bash

renderaccount="root"

#https://switch2osm.org/manually-building-a-tile-server-16-04-2-lts/

runuser -l postgres -c 'createuser --pwprompt root'

#sudo useradd -m $renderaccount

#sudo passwd $renderaccount

mkdir ~/src && cd ~/src/

git clone git://github.com/openstreetmap/osm2pgsql.git

cd osm2pgsql

sudo apt-get -y install make cmake g++ libboost-dev libboost-system-dev libboost-filesystem-dev libexpat1-dev zlib1g-dev libbz2-dev libpq-dev libgeos-dev libgeos++-dev libproj-dev lua5.2 liblua5.2-dev

mkdir build && cd build

cmake ..

make

sudo make install

sudo apt-get -y install autoconf apache2-dev libtool libxml2-dev libbz2-dev libgeos-dev libgeos++-dev libproj-dev gdal-bin libgdal1-dev libmapnik-dev mapnik-utils python-mapnik

cd ~/src

git clone git://github.com/SomeoneElseOSM/mod_tile.git

cd mod_tile

./autogen.sh

./configure.sh

make

sudo make install

sudo make install-mod_tile

sudo ldconfig

cd ~/src

git clone git://github.com/gravitystorm/openstreetmap-carto.git

cd openstreetmap-carto

sudo apt install npm nodejs-legacy

sudo npm install -g carto

carto project.mml > mapnik.xml

mkdir ~/data && cd ~/data

###obs -> download das regiões escolhidas

###wget http://download.geofabrik.de/south-america/brazil-latest.osm.pbf
###wget http://download.geofabrik.de/south-america/paraguay-latest.osm.pbf

### <- obs

###osm2pgsql -d gis --create --slim  -G --hstore --tag-transform-script ~/src/openstreetmap-carto/openstreetmap-carto.lua -C 2500 --number-processes 1 -S ~/src/openstreetmap-carto/openstreetmap-carto.style ~/data/brazil-latest.osm.pbf

osm2pgsql -d gis --create --slim -G --hstore --tag-transform-script ~/src/openstreetmap-carto/openstreetmap-carto.lua -C 2500 --number-processes 2 -S ~/src/openstreetmap-carto/openstreetmap-carto.style ~/data/itaipu.pbf
